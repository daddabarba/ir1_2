import read_ap
from argparse import ArgumentParser
from random import shuffle

import torch
from torch import nn
import numpy as np
from numpy import int32

from benchmarking import run_benchmarks

from math import ceil

import os

from vocab import generate_V

class SkipGram(nn.Module):

    def __init__(self, vocab, em_dim=300):
        super().__init__()
        self._vocab = vocab
        self._em_dim = em_dim

        self.w = nn.Embedding(self._vocab.vocab_size(), self._em_dim, sparse=True)
        self.c = nn.Embedding(self._vocab.vocab_size(), self._em_dim, sparse=True)

    def get_embedding(self, word):
        return self.w(word).squeeze().cpu().detach().numpy()

    def get_vocab(self):
        return self._vocab

    def get_em_dim(self):
        return self._em_dim

    def forward(self, word, context):
        word_em = self.w(word)
        context_em = self.c(context)

        # Return cosine similarity between context and token's embedding
        return (word_em * context_em).sum(axis=1)

def getEmbeddings(args):

    V = generate_V(args)

    if os.path.exists(f"{args.em_file}"):

        print("Pre-trained model found, loading embeddings.pt")

        with open(f"{args.em_file}", 'rb') as f:
            model = torch.load(f)
    else:

        print("\nBegin training")

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        model = SkipGram(V, args.em_dim).to(device)

        optimizer = torch.optim.SparseAdam(model.parameters(), lr=args.lr)

        for epoch in range(args.n_epochs):

            avg_pos_similarity = 0
            avg_neg_similarity = 0

            for i, (batch_pos, batch_neg) in enumerate(V.get_batches()):

                # Split inputs/outputs

                tokens_pos = torch.Tensor([x[0] for x in batch_pos]).to(torch.int64).to(device)
                context_pos = torch.Tensor([x[1] for x in batch_pos]).to(torch.int64).to(device)

                tokens_neg = torch.Tensor([x[0] for x in batch_neg]).to(torch.int64).to(device)
                context_neg = torch.Tensor([x[1] for x in batch_neg]).to(torch.int64).to(device)

                # Do not accumulate gradients
                optimizer.zero_grad()

                # Compute similarity between tokens and contexts
                pos_similarity = model(tokens_pos, context_pos)
                neg_similarity = model(tokens_neg, context_neg)

                # Compute loss for positive and negative samples separately
                pos_loss = torch.sigmoid(-pos_similarity).mean(axis=0)
                neg_loss = torch.sigmoid(neg_similarity).mean(axis=0)

                # Compute total loss
                loss = pos_loss + neg_loss

                # Backpropagate
                loss.backward()

                # Update weights
                optimizer.step()

                print("Epoch: %d %s Batch: %d/%d Similarity pos: %.2f Similarity neg: %.2f\t\t\r" % (epoch, "[" + "="*int((i+1)/ceil(len(V.pairs)/args.b_size)*100) + " "*(100-int((i+1)/ceil(len(V.pairs)/args.b_size)*100)) + "]", i+1, ceil(len(V.pairs)/args.b_size), 1-pos_loss, neg_loss), end="")

                avg_pos_similarity += 1-pos_loss
                avg_neg_similarity += neg_loss

            avg_pos_similarity /= ceil(len(V.pairs)/args.b_size)
            avg_neg_similarity /= ceil(len(V.pairs)/args.b_size)
            print("Epoch: %d %s Batch: %d/%d Similarity pos: %.2f Similarity neg: %.2f\t\t" % (epoch, "[" + "="*100 + "]" , i, ceil(len(V.pairs)/args.b_size), avg_pos_similarity, avg_neg_similarity))

        print("\nDone trianing")

        print("Serializing trained model in embeddings.pt")
        with open(f"{args.em_file}", "wb") as f:
            torch.save(model, f)

    return model, V

def doc2vec(model, document, device=torch.device("cuda" if torch.cuda.is_available() else "cpu"), aggregation_fn=lambda x: np.mean(x, axis=0)):
    tokens2vecs = []

    # for each token
    for token in document:

        # check if in dict word 2 id
        if token not in model.get_vocab().w2i:
            # if never seen before let its embedding not contribute to doc vector 
            tokens2vecs.append(np.zeros(model.get_em_dim(), dtype=int))
            continue

        # else get embedding
        embed = model.get_embedding(torch.LongTensor([model.get_vocab().w2i[token]]).to(device))
        # add to list for later aggregation
        tokens2vecs.append(embed)

    # return doc aggregated vector from all word2vecs
    return aggregation_fn(np.asarray(tokens2vecs))

def rank_batch(model, documents, query_vec, top_n, words):
    doc_ranks = []

    # transform docs to vecs
    for doc_id, doc in documents.items():

        # convert doc to vector from
        doc_vec = doc2vec(model, doc)
        query_vec = doc2vec(model, query_vec)

        # do inner product to obtain cos similarity 
        similarity = np.inner(doc_vec.astype(float), query_vec.astype(float))

        # first few words of document to print later
        text = ' '.join(documents[doc_id][:words])

        # add tuple to list
        doc_ranks.append((doc_id, similarity, text))

    # return sotred by similarity in descending order
    return sorted(doc_ranks, key=lambda x: x[1], reverse=True)[:top_n]

def rank_documents(model, query_vec, args, top_n=10, words=20):
    # list to keep the top n ranks over all batches
    top_ranks = []

    # for each batch
    for data_batch in range(args.n_data_batches):
        print("Ranking batch %d" % data_batch)

        # get documents in batch
        docs = read_ap.get_processed_docs(n_batches=args.n_data_batches, batches=[data_batch])

        # if top_ranks is empty rank batch and assume top n of batch
        # are the top n ranks overall
        if len(top_ranks) == 0:
            top_ranks = rank_batch(model, docs, query_vec, top_n, words)
            continue

        # else get ranked batch
        new_ranks = rank_batch(model, docs, query_vec, top_n, words)

        # merge the two, rerank and keep inly top n
        top_ranks = sorted(top_ranks + new_ranks, key=lambda x: x[1], reverse=True)[:top_n]

    return top_ranks

def cos_sim(x, y):
    return np.inner(x, y) / (np.linalg.norm(x) * np.linalg.norm(y))

def word_find_similar(embeddings, word, topn):
    sims = list(map(lambda x : (x[0], cos_sim(x[1], word)), embeddings.items()))

    return sorted(sims, reverse=True, key=lambda x: x[1])[:topn]

def all_similarities(model, words, topn=10):
    
    embeddings = dict(map(
            lambda x: (x, model.get_embedding(torch.LongTensor([model.get_vocab().w2i[x]]).to(torch.device("cuda" if torch.cuda.is_available() else "cpu")))), 
            model.get_vocab().w2i.keys()
        ))

    words = read_ap.process_text(words)

    for word in words:
        if word in model.get_vocab().w2i:
            word_vec = model.get_embedding(torch.LongTensor([model.get_vocab().w2i[word]]).to(torch.device("cuda" if torch.cuda.is_available() else "cpu")))
        else:
            print("''%s'' not in vocab will be skipped" % word)
            continue

        sims = word_find_similar(embeddings, word_vec, topn)
        print("Top ", topn, " words similar to ", word)
        print(sims) # TODO: make pretty print

def main(args):

    model, V = getEmbeddings(args)

    if args.rank:
        # convert query to vector
        query = doc2vec(model, read_ap.process_text(args.query))

        # if query is all zeros
        if np.count_nonzero(query) == 0:
            # none of the words are in the vocab
            # end ranking
            print("No documents match query")
            return

        # get ranks
        ranks = rank_documents(model, query, args, top_n=10)
        for rank in ranks:
            print(rank)



    if args.evaluate is not None:

        print("Running Benchmarks for doc2vec model")
        run_benchmarks(lambda query_text : {k:float(v) for k,v in rank_documents(model, query_text, args, top_n=None)}, args.evaluate, args.benchmarks_dataset, args.trecfile)

    if args.find_similar:
        all_similarities(model, args.query)

if __name__ == "__main__":

    parser = ArgumentParser()

    parser.add_argument("--k", type=int, help="Context window size", default=5)
    parser.add_argument("--em_dim", type=int, help="Embedding dimension", default=100)
    parser.add_argument("--lr", type=float, help="Learning rate", default=0.001)
    parser.add_argument("--b_size", type=int, help="Batch size", default=32)
    parser.add_argument("--min_freq", type=int, help="Min frequency for tokens", default=50)
    parser.add_argument("--n_pairs", type=int, help="Number of pairs selected", default=None)
    parser.add_argument("--dict_size", type=int, help="Number of tokens sampled", default=5000)
    parser.add_argument("--n_data_batches", type=int, help="Number of batches in which to split the data to be process in a memory efficent way ", default=20)
    parser.add_argument("--n_epochs", type=int, help="Number of epochs to train the model for", default=1)
    parser.add_argument("--query", type=str, help="Query text", default="top-secret high-grossing good-performance")
    parser.add_argument("--vocab_file", type=str, help="File in which to save serialized vocab", default="./vocabs/vocab.pkl")
    parser.add_argument("--em_file", type=str, help="file in which to serialize embeddings", default="./embeddings/embeddings.pt")
    parser.add_argument("--rank", type= lambda x : x.lower()=="true" if x.lower()=="true" or x.lower()=="false" else None, help="If true, ranks", default=False)
    parser.add_argument("--find_similar", type= lambda x : x.lower()=="true" if x.lower()=="true" or x.lower()=="false" else None, help="If true, find words similar to tokens given in string", default=False)
    parser.add_argument("--evaluate", type=str, help="If given, it runs benchamrks, --query is ignored, and the value of this parameter is sued as the output file for the benchmark", default=None)
    parser.add_argument("--benchmarks_dataset", type=str, help="Dataset to use for benchmarks", default="validation")
    parser.add_argument("--trecfile", type=str, help="If given, this is the name in which the TREC style result file is saved for a benchmark run", default=None)

    main(parser.parse_args())

