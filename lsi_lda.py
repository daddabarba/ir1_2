import gensim
from argparse import ArgumentParser

import pickle
import os
from gensim.matutils import kullback_leibler

import read_ap
from benchmarking import run_benchmarks

# Models

def train_lsi(corpus, dictionary, n_topics, model_type="bow"):
    if model_type == "tfidf":
        tfidf = gensim.models.TfidfModel(corpus)
        corpus = tfidf[corpus]

    return gensim.models.LsiModel(corpus=corpus, num_topics=n_topics, id2word=dictionary)

def train_lda(corpus, dictionary, num_topics, passes=1, iterations=400):
    # return gensim.models.LdaMulticore(
    return gensim.models.LdaModel(
        corpus=corpus,
        id2word=dictionary,
        alpha='auto',
        eta='auto',
        iterations=iterations,
        num_topics=num_topics,
        passes=passes,
        eval_every=None,
        # minimum_phi_value=0.0001,
        # per_word_topics=False
    )

# Ranking

def rank_lsi(model, query, dictionary, index, top_n=None):

    # get query tokens
    query = read_ap.process_text(query)

    # query doc2bow
    query_vec = dictionary.doc2bow(query)

    # convert to model space
    p_query = model[query_vec]

    sims = index[p_query]

    return sorted(enumerate(sims), key=lambda item: -item[1])[:top_n]

def rank_lda(model, query, docs, top_n=None):
    # p_query = process_query(model, query, model.id2word)
    # get query tokens
    query = read_ap.process_text(query)

    # query doc2bow
    query_vec = model.id2word.doc2bow(query)

    p_query = model.get_document_topics(query_vec, minimum_probability=0)

    ranks = []

    for doc_id, doc in docs.items():
        ranks.append((doc_id, float(kullback_leibler(doc, p_query))))

    return sorted(ranks, key=lambda x: x[1], reverse=True)[:top_n]

# Topics

def get_ranks(model, query, dictionary, index, docs_keys, top_n=10):
    ranks = rank_lsi(model, query, dictionary, index, top_n)

    for i, (dict_doc_id, score) in enumerate(ranks):
        print("%2d. (Doc.: %s, %.4f)" % ((i + 1), docs_keys[dict_doc_id], score))

def top_topics(model, index, dictionary, docs_keys, title, args):
    topics_pretty(model, title, n_topics=args.n_topics, n_words=args.n_words)
    get_ranks(model, args.query, dictionary, index, docs_keys)

# Benchmarks

def benchmark_lsi(model, dictionary, index, docs_keys, args):
    run_benchmarks(
        lambda query_text : {
            docs_keys[i]:float(s) for i,s in
            rank_lsi(
                model,
                query_text,
                dictionary,
                index,
                len(docs_keys)
            )
        },
        args.evaluate,
        args.benchmarks_dataset,
        args.trecfile
    )

def benchmark_lda(model, doc_vecs, args):
    run_benchmarks(
        lambda query_text : dict(
            rank_lda(
                model,
                query_text,
                doc_vecs,
                args.topics_to_fit
            )
    ),
    args.evaluate,
    args.benchmarks_dataset,
    args.trecfile
    )

# Helpers

def topics_pretty(model, model_name, n_topics=5, n_words=5):
    print("\nTop %d topics for model: %s" % (n_topics, model_name))
    for topic in model.print_topics(num_topics=n_topics, num_words=n_words):
        print("%d: %s" % (topic[0], topic[1]))
    print()




def main(args):
    if os.path.exists(args.corpus) and os.path.exists(args.dictionary):

        with open(args.corpus, "rb") as f:
            corpus = pickle.load(f)

        with open(args.dictionary, "rb") as f:
            dictionary = pickle.load(f)
    else:

        dictionary = gensim.corpora.Dictionary()
        corpus = []

        # Get documents dictionary
        for data_batch in range(args.n_data_batches):
            print("\tprocessing dict for batch %d ..."%data_batch)
            docs = read_ap.get_processed_docs(n_batches=args.n_data_batches, batches=[data_batch])

            dictionary.add_documents(docs.values())

        # filter out non-frequent words
        dictionary.filter_extremes(no_below=args.min_freq)

        for data_batch in range(args.n_data_batches):
            print("\tprocessing corpus for batch %d ..."%data_batch)
            docs = read_ap.get_processed_docs(n_batches=args.n_data_batches, batches=[data_batch])

            corpus += [dictionary.doc2bow(doc) for doc in docs.values()]

        with open(args.corpus, "wb") as f:
            pickle.dump(corpus, f)

        with open(args.dictionary, "wb") as f:
            pickle.dump(dictionary, f)

    # train different models
    if args.lsi_tfidf or args.lsi_bow:
        docs_keys = []

        for data_batch in range(args.n_data_batches):
            print("\tprocessing doc keys for batch %d ..."%data_batch)
            docs = read_ap.get_processed_docs(n_batches=args.n_data_batches, batches=[data_batch])

            docs_keys += list(docs.keys())


        model_type="bow" if args.lsi_bow else "tfidf"

        print("Training LSI model: ")
        lsi_model = train_lsi(corpus, dictionary, args.topics_to_fit, model_type=model_type)
        print("Training LSI done!")

        # transform corpus to LSI space and index it
        print("Computing LSI similarity matrix")
        index = gensim.similarities.MatrixSimilarity(lsi_model[corpus])
        print("Computing LSI similarity matrix done!")

        if args.evaluate is not None:
            benchmark_lsi(lsi_model, dictionary, index, docs_keys, args)
        else:
            top_topics(lsi_model, index, dictionary, docs_keys, "LSI " + model_type, args)

    elif args.lda:

        if not os.path.exists('./lda.pkl'):
            print("Training LDA model: ")
            lda_model = train_lda(corpus, dictionary, args.topics_to_fit, passes=args.passes, iterations=args.iterations)
            print("Training LDA done!")

            with open('./lda.pkl', 'wb') as f:
                pickle.dump(lda_model, f)

        else:
            with open('./lda.pkl', 'rb') as f:
                lda_model = pickle.load(f)

        if args.evaluate is not None:
            if os.path.exists(args.lda_docs):

                with open(args.lda_docs, "rb") as f:
                    lda_doc_vecs = pickle.load(f)
            else:

                lda_doc_vecs = {}

                for data_batch in range(args.n_data_batches):
                    print("\tprocessing doc keys for batch %d ..."%data_batch)
                    docs = read_ap.get_processed_docs(n_batches=args.n_data_batches, batches=[data_batch])

                    for doc_id, doc in docs.items():
                        assert doc_id not in lda_doc_vecs

                        lda_doc_vecs[doc_id] = lda_model[lda_model.id2word.doc2bow(doc)]

                with open(args.lda_docs, "wb") as f:
                    pickle.dump(lda_doc_vecs, f)

            benchmark_lda(lda_model, lda_doc_vecs, args)
        else:
            topics_pretty(lda_model, "LDA")

if __name__ == "__main__":


    parser = ArgumentParser()

    parser.add_argument("--min_freq", type=int, help="Min frequency for tokens", default=50)
    parser.add_argument("--n_topics", type=int, help="Number of topics to print", default=5)
    parser.add_argument("--topics_to_fit", type=int, help="Number of topics to fit", default=500)
    parser.add_argument("--n_words", type=int, help="Number of words for model", default=5)
    parser.add_argument("--passes", type=int, help="Number of passes for LDA", default=1)
    parser.add_argument("--iterations", type=int, help="Number of iterations for LDA", default=400)
    parser.add_argument("--query", type=str, help="Query to rank documents on", default="Bank of America")
    parser.add_argument("--lsi_bow", type= lambda x : x.lower()=="true" if x.lower()=="true" or x.lower()=="false" else None, help="If true, runs LSI model with BOW", default=False)
    parser.add_argument("--lsi_tfidf", type= lambda x :  x.lower()=="true" if x.lower()=="true" or x.lower()=="false" else None, help="If true, runs LSI model with TF-IDF", default=False)
    parser.add_argument("--lda", type= lambda x : x.lower()=="true" if x.lower()=="true" or x.lower()=="false" else None, help="If true, runs LDA", default=False)
    parser.add_argument("--evaluate", type=str, help="If given, it runs benchamrks, --query is ignored, and the value of this parameter is sued as the output file for the benchmark", default=None)
    parser.add_argument("--benchmarks_dataset", type=str, help="Dataset to use for benchmarks", default="validation")
    parser.add_argument("--trecfile", type=str, help="If given, this is the name in which the TREC style result file is saved for a benchmark run", default=None)
    parser.add_argument("--n_data_batches", type=int, help="Number of batches in which to split the data to be process in a memory efficent way ", default=20)
    parser.add_argument("--corpus", type=str, help="Files in which to serialize (or from which to read) pre computed corpus", default="./corpus.pkl")
    parser.add_argument("--dictionary", type=str, help="Files in which to serialize (or from which to read) pre computed dictionary", default="./dict.pkl")
    parser.add_argument("--lda_docs", type=str, help="Docs passed through LDA model", default="./lda_docs.pkl")


    main(parser.parse_args())
