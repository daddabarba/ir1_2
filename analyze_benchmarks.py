import json
from argparse import ArgumentParser


if __name__ == "__main__":

    parser = ArgumentParser()

    parser.add_argument("--filename", type=str, help="Name ofg json file to load")
    parser.add_argument("--start", type=int, help="Limits the queries used", default=None)
    parser.add_argument("--to", type=int, help="Limits the queries used", default=None)

    args = parser.parse_args()

    with open(args.filename, 'r') as f:
        results = json.load(f)

    cumulative_map = 0
    cumulative_ndcg = 0

    for q_id, data in list(results.items())[args.start:args.to]:

        cumulative_map += data["map"]
        cumulative_ndcg += data['ndcg']


    n = len(list(results.items())[args.start:args.to])
    print("Cumulative mAP: ", cumulative_map, "(mean: ", cumulative_map/n, ") Cumulative ndcg: ", cumulative_ndcg, "(mean: ", cumulative_ndcg/n, ")")

