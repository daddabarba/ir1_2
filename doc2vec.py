import gensim
from argparse import ArgumentParser

import read_ap
from benchmarking import run_benchmarks

def main(args, workers=4):

    # convert docs to TaggedDocument
    tagged_docs = []

    for data_batch in range(args.n_data_batches):
            print("\tprocessing batch %d ..."%data_batch)
            docs = read_ap.get_processed_docs(n_batches=args.n_data_batches, batches=[data_batch])

            tagged_docs_batch = list(map(lambda x : gensim.models.doc2vec.TaggedDocument(x[1],[x[0]]), docs.items()))
            tagged_docs += tagged_docs_batch

    # build model
    model = gensim.models.doc2vec.Doc2Vec(
        vector_size=args.em_dim,
        min_count=50,
        window=args.k,
        workers=workers,
        max_vocab_size=args.dict_size
    )

    # create vocab from docs
    print("Building vocabulary")
    model.build_vocab(tagged_docs)

    # train model
    print("Training doc2vec model")
    model.train(tagged_docs, total_examples=model.corpus_count, epochs=model.epochs)

    if args.evaluate is not None:

        print("Running Benchmarks for doc2vec model")
        run_benchmarks(lambda query_text : {k:float(v) for k,v in rank(model, query_text, len(model.docvecs))}, args.evaluate, args.benchmarks_dataset, args.trecfile)

    else:
        # rank according to query
        ranks = rank(model, args.query, args.n_ranks)

        # display ranks
        for i, ranked_doc in enumerate(ranks):
            doc_id = ranked_doc[0]
            similarity = float(ranked_doc[1])
            print("%d. (%s, %.4f)\n" % (i + 1, doc_id, similarity))

def rank(model, query, n):
    # tokenize query
    query = read_ap.process_text(query)

    # get infer vector of query
    inf_vector = model.infer_vector(query)

    # get top n documents most similar to query
    sims = model.docvecs.most_similar([inf_vector], topn=n)

    return sims

if __name__ == "__main__":

    parser = ArgumentParser()

    parser.add_argument("--k", type=int, help="Context window size", default=5)
    parser.add_argument("--em_dim", type=int, help="Embedding dim", default=100)
    parser.add_argument("--n_ranks", type=int, help="Number of ranks to display", default=10)
    parser.add_argument("--query", type=str, help="Query to rank on", default="Bancorporation of Texas")
    parser.add_argument("--evaluate", type=str, help="If given, it runs benchamrks, --query is ignored, and the value of this parameter is sued as the output file for the benchmark", default=None)
    parser.add_argument("--benchmarks_dataset", type=str, help="Dataset to use for benchmarks", default="validation")
    parser.add_argument("--trecfile", type=str, help="If given, this is the name in which the TREC style result file is saved for a benchmark run", default=None)
    parser.add_argument("--workers", type=int, help="Number of workers", default=4)
    parser.add_argument("--dict_size", type=int, help="Number of tokens sampled", default=None)
    parser.add_argument("--n_data_batches", type=int, help="Number of batches in which to split the data to be process in a memory efficent way ", default=20)

    args = parser.parse_args()
    model = main(args, args.workers)
