#!/bin/bash

# Run benchmarks with default values for word2vec
python3 word2vec.py --em_file ./embeddings/embeddings_default.pkl --vocab_file ./vocabs/vocab_default.pkl --k 5 --em_dim 100 --n_pairs 300000 --b_size 1000 --n_epochs 1000 --evaluate ./benchmarks/w2v_default.json --benchmarks_dataset all

# Run benchmarks with default values for doc2vec
python3 doc2vec.py --workers 12  --k 5 --em_dim 100 --evaluate ./benchmarks/d2v_default.json --benchmarks_dataset all

# Run benchmarks with default values for lsi bow
python3 lsi_lda.py --lsi_bow True --topics_to_fit 500 --evaluate ./benchmarks/lsi_bow_default.json --benchmarks_dataset all

# Run benchmarks with default values for lsi tf-idf
python3 lsi_lda.py --lsi_tfidf True --topics_to_fit 500 --evaluate ./benchmarks/lsi_tfidf_default.json --benchmarks_dataset all

# Run benchmarks with default values for lda
python3 lsi_lda.py --lda True --topics_to_fit 500 --evaluate ./benchmarks/lda_default.json --benchmark_dataset all
