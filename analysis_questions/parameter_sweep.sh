#!/bin/bash

# doc2vec

## vector dimension
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_ED_200.json --em_dim 200 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_ED_300.json --em_dim 300 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_ED_400.json --em_dim 400 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_ED_500.json --em_dim 500 --benchmarks_dataset all

## window size
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_K_5.json --k 5 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_K_10.json --k 10 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_K_15.json --k 15 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_K_20.json --k 20 --benchmarks_dataset all

## vocab size
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_VS_10k.json --dict_size 10000 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_VS_25k.json --dict_size 25000 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_VS_50k.json --dict_size 50000 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_VS_100k.json --dict_size 100000 --benchmarks_dataset all
python3 doc2vec.py --workers 12 --evaluate ./benchmarks/d2v_VS_200k.json --dict_size 200000 --benchmarks_dataset all

# lsi bow
python3 lsi_lda.py --lsi_bow True --evaluate ./benchmarks/lsi_10.json --topics_to_fit 10 --benchmarks_dataset all
python3 lsi_lda.py --lsi_bow True --evaluate ./benchmarks/lsi_50.json --topics_to_fit 50 --benchmarks_dataset all
python3 lsi_lda.py --lsi_bow True --evaluate ./benchmarks/lsi_100.json --topics_to_fit 100 --benchmarks_dataset all
python3 lsi_lda.py --lsi_bow True --evaluate ./benchmarks/lsi_500.json --topics_to_fit 500 --benchmarks_dataset all
python3 lsi_lda.py --lsi_bow True --evaluate ./benchmarks/lsi_1000.json --topics_to_fit 1000 --benchmarks_dataset all
python3 lsi_lda.py --lsi_bow True --evaluate ./benchmarks/lsi_2000.json --topics_to_fit 2000 --benchmarks_dataset all
