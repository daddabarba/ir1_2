#!/bin/bash

python3 word2vec.py --em_file ./embeddings/embeddings_50k_100k.pkl --vocab_file ./vocabs/vocab_50k_100k.pkl --dict_size 50000 --n_pairs 100000 --b_size 1000 --n_epochs 500 --find_similar True --query "ketamine blackmarket songman weapn bambu"
