#!/bin/bash

# Run test with optimal parameters for word2vec
python3 word2vec.py --em_file ./embeddings/embeddings_best.pkl --vocab_file ./vocabs/vocab_best.pkl --k 10 --dict_size 200000 --em_dim 200 --n_pairs 300000 --b_size 1000 --n_epochs 1000 --evaluate ./benchmarks/w2v_best.json --trecfile ./trecfiles/w2v_best.txt --benchmarks_dataset all

# Run test with optimal parameters for doc2vec
python3 doc2vec.py --workers 12  --k 10 --dict_size 200000 --em_dim 200 --evaluate ./benchmarks/d2v_best.json --trecfile ./trecfiles/d2v_best.txt --benchmarks_dataset all

# Run test with optimal parameter for lsi bow
python3 lsi_lda.py --lsi_bow True --topics_to_fit 2000 --evaluate ./benchmarks/lsi_bow_best.json --trecfile ./trecfiles/lsi_bow_best.txt --benchmarks_dataset all

# Run test with optimal parameter for lsi tf-idf
python3 lsi_lda.py --lsi_tfidf True --topics_to_fit 2000 --evaluate ./benchmarks/lsi_tfidf_best.json --trecfile ./trecfiles/lsi_tfidf_best.txt --benchmarks_dataset all

# Run test with optimal parameters for lda
python3 lsi_lda.py --lda True --topics_to_fit 2000 --evaluate ./benchmarks/lda_best.json --trecfile ./trecfiles/lda_best.txt --benchmarks_dataset all
