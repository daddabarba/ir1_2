#!/bin/bash

python3 lsi_lda.py --lsi_bow True
python3 lsi_lda.py --lsi_tfidf True
python3 lsi_lda.py --lda True
