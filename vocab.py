import read_ap
from argparse import ArgumentParser
from random import shuffle

import torch
from torch import nn
from numpy import int32
import random

import os
import pickle

from math import ceil

class Vocab:

    def __init__(self, args):

        # tokens dicts
        self.w2i = {} # init token to id dictionary
        self.i2w = {} # init id to token dictionary

        self.freqs = {}

        self.pairs = {} # init context pairs dict

        self.set_args(args)

    def set_args(self, args):
        # This method is mainly for changing the hyperparameters if loading serialized vocab

        # hyperparameters
        self.batch_size = args.b_size # batch size to yield
        self.dict_size = args.dict_size # numbers of tokens to keep
        self.n_pairs = args.n_pairs # numbers of pairs to sample
        self.min_freq = args.min_freq # threshold to eliminate low freq tokens
        self.k = args.k # context window size

        return self

    def get_tokens_freq(self, docs):
        # Update tokens frequencies

        # Get single words frequebcies
        for doc in docs.values():
            for token in doc:

                # check if token already exists
                if token not in self.w2i:

                    i = len(self.i2w)

                    # add token to mappings
                    self.w2i[token] = i
                    self.i2w[i] = token

                    self.freqs[i] = 1

                else:
                    self.freqs[self.w2i[token]] += 1

    def generate_dictionary(self):
        # Select tokens according to frequency

        dictionary = sorted(self.freqs.items(), reverse=True, key=lambda x: x[1])[:self.dict_size]
        dictionary = dict(dictionary)

        # Reconstruct vocab
        new_w2i = {}
        new_i2w = {}

        for i in self.i2w.keys():
            if i in dictionary:

                token = self.i2w[i]
                new_i = len(new_i2w)

                new_w2i[token] = new_i
                new_i2w[new_i] = token

        self.w2i = new_w2i
        self.i2w = new_i2w

    def get_pairs_freq(self, docs):
        # Update token pairs frequencies

        # Compute offset for context window
        offset = self.k

        # Get number of documents
        ndocs = len(docs)

        # Init progress counter
        docs_processed = 0

        # Get pairs frequencies
        for doc_tokens in docs.values():

            print("Completed %d/%d (%.0f"%(docs_processed, ndocs, docs_processed/ndocs*100), chr(37), ")\t\r", end="")

            # precompute upper limit for context
            l = len(doc_tokens)

            # process all context windows
            for center in range(l):
                window = doc_tokens[max(0, center-offset):min(l, center+offset+1)]
                token = doc_tokens[center]

                # for each token in the context window
                for c_token in window:

                    # that is not the token whose context we observed
                    if c_token is not token and c_token in self.w2i and token in self.w2i:
                        pair = (self(token), self(c_token)) # add pair to context data

                        if pair not in self.pairs:
                            self.pairs[pair] = 1
                        else:
                            self.pairs[pair] += 1

            docs_processed += 1

        print("")

    def generate_pairs(self):

        self.pairs = sorted(self.pairs.items(), reverse=True, key=lambda x: x[1])[:self.n_pairs]
        self.pairs = list(dict(self.pairs).keys())

    def generate_neg_pairs(self):

        self.neg_pairs = [0]*self.n_pairs # init empty list

        # Generate a negative pair (t, cn) for each pair (t, c)
        for i, (t, _) in enumerate(self.pairs):

            # Sample a possible context
            [(_, c)] = random.sample(self.pairs, 1)

            # Find context not associated to token
            while (t,c) in self.pairs:
                [(_, c)] = random.sample(self.pairs, 1)

            self.neg_pairs[i] = (t,c)

            print("Found %d/%d negative pairs\t\t\r"%(i, self.n_pairs), end="")

        print(" ")


    # Getters
    def __call__(self, token):
        # Return id of token
        return self.w2i[token]

    def vocab_size(self):
        # Number of tokens observed
        return len(self.w2i)

    def num_pairs(self):
        # Number of pairs observed
        return len(self.pairs)

    # Data API
    def get_batches(self):
        # splits dataset in usable batches for nn training

        paired = list(zip(self.pairs, self.neg_pairs))
        shuffle(paired)

        self.pairs, self.neg_pairs = zip(*paired)

        for i in range(0, len(self.pairs), self.batch_size):
            yield self.pairs[i:min(i+self.batch_size, len(self.pairs))], self.neg_pairs[i:min(i+self.batch_size, len(self.pairs))]

def generate_V(args):

    if os.path.exists(f"{args.vocab_file}"):
        with open(f"{args.vocab_file}", 'rb') as f:
            return pickle.load(f).set_args(args)
    else:

        # Init vocab
        V = Vocab(args)

        # Process each documents batch

        print("Processing vocab")

        # Get per token frequencies

        print("Processing single tokens frequencies")

        for data_batch in range(args.n_data_batches):
            print("\tprocessing batch %d ..."%data_batch)
            docs = read_ap.get_processed_docs(n_batches=args.n_data_batches, batches=[data_batch])
            V.get_tokens_freq(docs)

        print("Sampling tokens")
        V.generate_dictionary()

        print("Processing token pairs frequencies")
        for data_batch in range(args.n_data_batches):
            print("\tprocessing batch %d ..."%data_batch)
            docs = read_ap.get_processed_docs(n_batches=args.n_data_batches, batches=[data_batch])
            V.get_pairs_freq(docs)

        print("Sampling pairs")
        V.generate_pairs()

        print("Sampling negative pairs")
        V.generate_neg_pairs()

        print("\n Done processing Vocab object\tvocab size: ", V.vocab_size(), "\tnum pairs: ", V.num_pairs())

        with open(f"{args.vocab_file}", 'wb') as f:
            pickle.dump(V, f)

        return V
