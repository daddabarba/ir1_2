import pytrec_eval
from tqdm import tqdm

import read_ap
import json

# read in the qrels
qrels_tot, queries_tot = read_ap.read_qrels()

qrels_tot = list(qrels_tot.items())
queries_tot = list(queries_tot.items())

qrels_val = dict(qrels_tot[76:100])
queries_val = dict(queries_tot[76:100])

qrels_test = dict(qrels_tot[:76] + qrels_tot[101:])
queries_test = dict(queries_tot[:76] + queries_tot[101:])

qrels_tot = dict(qrels_tot)
queries_tot = dict(queries_tot)

def run_benchmarks(model, output, dataset="validation", trecfile=None):

    assert dataset.lower()=="validation" or dataset.lower()=="test" or dataset.lower()=="all", "Dataset for benchmarks can be either validation, test, or all"

    if dataset.lower()=="all":
        qrels = qrels_tot
        queries = queries_tot
    elif dataset.lower()=="validation":
        qrels = qrels_val
        queries = queries_val
    else:
        qrels = qrels_test
        queries = queries_test


    # model's rankings dict
    overall_ser = {}

    # collect results
    for qid in tqdm(qrels):

        query_text = queries[qid]

        results = model(query_text)
        overall_ser[qid] = dict(results)

    # run evaluation with `qrels` as the ground truth relevance judgements
    # here, we are measuring MAP and NDCG, but this can be changed to
    # whatever you prefer
    evaluator = pytrec_eval.RelevanceEvaluator(qrels, {'map', 'ndcg'})
    metrics = evaluator.evaluate(overall_ser)

    # dump this to JSON
    # *Not* Optional - This is submitted in the assignment!
    with open(output, "w") as writer:
        json.dump(metrics, writer, indent=1)

    if trecfile is not None:
        make_trec(overall_ser, trecfile)

def make_trec(overall_ser, filename):

    with open(filename, 'w') as f:
        for qid, scores in overall_ser.items():
            for rank, (doc_id, doc_score) in enumerate(scores.items()):

                if rank>= 1000:
                    break

                f.write(str(qid) + " Q0 " + str(doc_id) + " " + str(rank) + " "  + str(doc_score) + " 1\n")

