import json
import numpy as np

if __name__ == "__main__":

    # add dirs in list doenst have to be pretty
    list_of_json_dirs = ["./benchmarks/lda_best.json", "./benchmarks/lsi_bow_best.json", "./benchmarks/lsi_tfidf_best.json", "./benchmarks/lsi_tfidf_best.json", "./benchmarks/d2v_best.json", "./benchmarks/w2v_best.json"]

    # sotre dicts
    loaded_jsons = []

    # load each json
    for json_file in list_of_json_dirs:
        with open(json_file) as f:
            loaded_jsons.append(json.load(f))

    # query_id to variance
    query_vars = {}

    # for each query
    for query_id in loaded_jsons[0].keys():
        
        variance = []

        # get result per json MAP
        for json_file in loaded_jsons:
            variance.append(json_file[query_id]["map"])

        # store variance
        query_vars[query_id] = np.var(variance)

    # sort and get top 5
    query_vars = sorted(query_vars.items(), reverse=True, key=lambda x: x[1])[:5]
